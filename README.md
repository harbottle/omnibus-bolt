# omnibus-bolt
[![build status](https://gitlab.com/harbottle/omnibus-bolt/badges/master/build.svg)](https://gitlab.com/harbottle/omnibus-bolt/builds)

Omnibus full-stack installers for [bolt](https://puppet.com/docs/bolt).

Download packages for:
- [CentOS 6](https://gitlab.com/harbottle/omnibus-bolt/-/jobs/artifacts/master/browse/packages/centos6?job=publish%3Aartifacts)
- [CentOS 7](https://gitlab.com/harbottle/omnibus-bolt/-/jobs/artifacts/master/browse/packages/centos7?job=publish%3Aartifacts)
- [Debian 8 (Jessie)](https://gitlab.com/harbottle/omnibus-bolt/-/jobs/artifacts/master/browse/packages/debian8jessie?job=publish%3Aartifacts)
- [Debian 9 (Stretch)](https://gitlab.com/harbottle/omnibus-bolt/-/jobs/artifacts/master/browse/packages/debian9stretch?job=publish%3Aartifacts)
- [Debian 10 (Buster)](https://gitlab.com/harbottle/omnibus-bolt/-/jobs/artifacts/master/browse/packages/debian10buster?job=publish%3Aartifacts)
- [Fedora 25](https://gitlab.com/harbottle/omnibus-bolt/-/jobs/artifacts/master/browse/packages/fedora25?job=publish%3Aartifacts)
- [Fedora 26](https://gitlab.com/harbottle/omnibus-bolt/-/jobs/artifacts/master/browse/packages/fedora26?job=publish%3Aartifacts)
- [Fedora 27](https://gitlab.com/harbottle/omnibus-bolt/-/jobs/artifacts/master/browse/packages/fedora27?job=publish%3Aartifacts)
- [macOS](https://gitlab.com/harbottle/omnibus-bolt/-/jobs/artifacts/master/browse/packages/macos?job=publish%3Aartifacts)
- [Red Hat Enterprise Linux 6](https://gitlab.com/harbottle/omnibus-bolt/-/jobs/artifacts/master/browse/packages/rhel6?job=publish%3Aartifacts)
- [Red Hat Enterprise Linux 7](https://gitlab.com/harbottle/omnibus-bolt/-/jobs/artifacts/master/browse/packages/rhel7?job=publish%3Aartifacts)
- [Ubuntu 14.04 (Trusty)](https://gitlab.com/harbottle/omnibus-bolt/-/jobs/artifacts/master/browse/packages/ubuntu14.04trusty?job=publish%3Aartifacts)
- [Ubuntu 16.04 (Xenial)](https://gitlab.com/harbottle/omnibus-bolt/-/jobs/artifacts/master/browse/packages/ubuntu16.04xenial?job=publish%3Aartifacts)
- [Ubuntu 17.04 (Zesty)](https://gitlab.com/harbottle/omnibus-bolt/-/jobs/artifacts/master/browse/packages/ubuntu17.04zesty?job=publish%3Aartifacts)
- [Ubuntu 17.10 (Artful)](https://gitlab.com/harbottle/omnibus-bolt/-/jobs/artifacts/master/browse/packages/ubuntu17.10artful?job=publish%3Aartifacts)

## Yum repo for CentOS 7 and RHEL 7 users

CentOS 7 and RHEL 7 users also have the option to install the package
using the [yum repo](https://gitlab.com/harbottle/omnibus):

```bash
# Install the repo:
sudo yum -y install https://harbottle.gitlab.io/omnibus/7/x86_64/omnibus-release.rpm

# Install bolt:
sudo yum -y install bolt
```
