name "bolt"
default_version "0.19.1"
skip_transitive_dependency_licensing true

dependency "ruby"
dependency "rubygems"

build do
  gem "install bolt -n #{install_dir}/bin --no-rdoc --no-ri -v #{version}"
  command "rm -rf /opt/bolt/embedded/docs"
  command "rm -rf /opt/bolt/embedded/share/man"
  command "rm -rf /opt/bolt/embedded/share/doc"
  command "rm -rf /opt/bolt/embedded/ssl/man"
  command "rm -rf /opt/bolt/embedded/info"
end
