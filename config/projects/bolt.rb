#
# Copyright 2017 harbottle
#
# All Rights Reserved.
#

name "bolt"
maintainer "harbottle"
homepage "https://gitlab.com/harbottle/centos7-bolt"
license 'APACHE-2.0'

# Defaults to C:/bolt on Windows
# and /opt/bolt on all other platforms
install_dir "#{default_root}/#{name}"

build_version '0.19.1'
build_iteration 1

# Creates required build directories
#dependency "preparation"

# bolt dependencies/components
# dependency "somedep"

# Version manifest file
#dependency "version-manifest"

dependency "bolt"
override :ruby, version: "2.3.7"


exclude "**/.git"
exclude "**/bundler/git"
